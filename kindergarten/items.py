import scrapy


class TableRow(scrapy.Item):
    kgnumber = scrapy.Field()
    group_name = scrapy.Field()
    age_bounds = scrapy.Field()
    places = scrapy.Field()
    queue = scrapy.Field()
    year = scrapy.Field()
