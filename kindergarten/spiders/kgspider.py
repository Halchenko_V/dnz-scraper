from datetime import datetime

import scrapy

from kindergarten.items import TableRow


class MySpider(scrapy.Spider):
    name = 'kgspider'

    start_urls = [
        "http://old.lutskrada.gov.ua/all-child-request"
    ]

    def parse(self, response):
        for number in response.xpath('//*[@id="edit-dnz"]/option/@value').getall()[1:]:
            yield self.make_request(
                number,
                datetime.now().year
            )

    def make_request(self, kgnumber, year):
        year -= 1 # adapting to wierd bug on the website
        url = 'http://old.lutskrada.gov.ua/all-child-request?dnz={}&group=1&num=&year%5Bvalue%5D={}&status=All'.format(kgnumber, year)
        return scrapy.Request(url=url,
                              meta={'kgnumber': kgnumber},
                              callback=self.parse_data)

    @staticmethod
    def parse_data(response):
        for row in [row.xpath('td/text()') for row in response.xpath('(//table)[1]/tbody/tr')[1:]]:
            group, age_bounds, places, queue = row
            yield TableRow(
                    kgnumber=response.request.meta['kgnumber'],
                    group_name=group.get(),
                    age_bounds=age_bounds.get(),
                    places=places.get(),
                    queue=queue.get(),
                    year=datetime.now().year,
            )

