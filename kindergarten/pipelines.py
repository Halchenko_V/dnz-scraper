# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import csv
from os.path import join
from datetime import datetime


class KindergartenPipeline(object):
    def __init__(self):
        self.table = []

    def process_item(self, item, spider):
        self.table.append(item)

    def close_spider(self, spider):
        current_datetime = datetime.now()
        filename = '{0.year}-{0.month}-{0.day}.csv'.format(current_datetime)

        output_path = join(spider.output_dir, filename)

        with open(output_path, 'w') as f:
            rows = [(row["kgnumber"],
                     row["group_name"],
                     row["age_bounds"],
                     row["places"],
                     row["queue"],
                     row["year"]) for row in self.table]
            rows = sorted(rows, key=lambda r: int(r[0])) # sort by
            rows = [("№ ДНЗ",
                     "Група",
                     "Вікові рамки",
                     "Всього місць в групі",
                     "Всього дітей в черзі",
                     "Рік вступу ")] + rows
            writer = csv.writer(f)
            writer.writerows(rows)
